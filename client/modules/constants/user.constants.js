(function() {
  'use strict';

  angular
    .module('roastlab')
    .constant('userSession', {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      joined: '',
      lastLogin: '',
      loggedIn: false
    });

})();
