(function(){
  'use strict';

var dateConvert = function($filter){
  return function(date) {
    var system_date = new Date();
    var diff = Math.floor((Date.parse(system_date) - Date.parse(date)) / 1000);
    if (diff <= 90) return '1 minute';
    if (diff <= 3540) return Math.round(diff / 60) + ' minutes';
    if (diff <= 5400) return '1 hour';
    if (diff <= 86400) return Math.round(diff / 3600) + ' hours';
    if (diff <= 129600) return '1 day';
    if (diff < 604800) return Math.round(diff / 86400) + ' days';
    else return $filter('date')(new Date(date), 'dd MMM');
  };
};

angular
  .module('roastlab')
  .filter('dateConvert', dateConvert);
})();
