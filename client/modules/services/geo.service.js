(function(){
'use strict';

var geoService = function($q, $timeout) {

  this.geocode = function(location) {
    var geocoder = new google.maps.Geocoder();
    var q = $q.defer();
    setTimeout(function() {
      geocoder.geocode({'address': location}, function(results, status) {
        q.resolve(results);
      });
    }, 100);
    return q.promise;
  }

  this.elevation = function(lat, lng) {
    var location = [{lat: lat, lng: lng}];
    var elevator = new google.maps.ElevationService();
    var q = $q.defer();
    setTimeout(function() {
      elevator.getElevationForLocations({ 'locations': location }, function(results, status) {
        q.resolve(results);
      });
    }, 100);
    return q.promise;
  }

  // this.geocode = function(location) { return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+ location + '&key=AIzaSyBaw75SCATckl56nVHGoReiwx3jxfq6ATE'); };
  // this.elevation = function(lat, lng) { return $http.get('https://maps.googleapis.com/maps/api/elevation/json?locations=' + lat + ',' + lng + '&key=AIzaSyBaw75SCATckl56nVHGoReiwx3jxfq6ATE'); };

};

angular
  .module('roastlab')
  .service('geoService', [
    '$q',
    '$timeout',
    geoService
  ]);

})();
