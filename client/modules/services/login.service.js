(function(){
'use strict';

var loginService = function($http) {

  this.login = function(user) { return $http.post('/api/login', user); };
  this.logout = function() { return $http.post('/api/logout'); };
  this.check = function() { return $http.get('/api/loggedin') };

};

angular
  .module('roastlab')
  .service('loginService', [
    '$http',
    loginService
  ]);

})();
