(function(){
'use strict';

var lotService = function($http) {

  this.getAll = function() { return $http.get('/api/lots'); };
  this.getOne = function(id) { return $http.get('/api/lot/' + id); };
  this.removeOne = function(id) { return $http.delete('/api/lot/' + id); };
  this.createOne = function(data) { return $http.post('/api/lot', data); };
  this.updateOne = function(id, data) { return $http.put('/api/lot/' + id, data); };

};

angular
  .module('roastlab')
  .service('lotService', [
    '$http',
    lotService
  ]);

})();
