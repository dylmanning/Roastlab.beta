(function() {
  'use strict';

  var lotController = function($state, userSession, lotService, loginService, geoService, NgMap, lot) {

    userSession.loggedIn = true;

    //set current lot
    this.lot = lot;
    //set current assesments
    this.assesments = lot.assesments;
    //set visits
    this.visits = lot.visits;

    //set current date
    this.date = new Date();

    //home
    this.home = function() {
      $state.go('dashboard');
    };

    //edit mode
    this.editing = false;
    this.edit = function() {
      this.editing = !this.editing;
    };

    this.addAssesment = function() {
      var self = this;
      self.assesments.push({name: 'Green', url: 'views/partials/green.view.html'});
    };

    //date
    this.open = function($event) {
      this.status.opened = true;
    };

    this.status = {
      opened: false
    };

    this.addVisit = function(date) {
      var self = this;
      self.visits.push({date: date});
    };

    this.removeVisit = function($index) {
      var self = this;
      self.visits.splice($index, 1);
    };

    //update
    this.update = function(lot) {
      var self = this;
      lotService.updateOne(lot._id, lot)
        .then(function(res) {
          //get lot
          lotService.getOne(lot._id)
            .then(function(res) {
              lot = res.data;
              //back to view mode
              self.editing = !self.editing;
            });
        });
    };

    //delete
    this.remove = function(id) {
      lotService.removeOne(id)
        .then(function(res) {
          //back to list view
          $state.go('dashboard');
        });
    };

    //hide empty fields
    this.isEmpty = function(obj) {
      for (var i in obj) if (obj.hasOwnProperty(i)) return false;
        return true;
    };

    //draw map
    var drawMap = function(map) {
      NgMap.getMap({id: 'map'})
      .then(function(map) {
          var myLatlng = new google.maps.LatLng(lot.geo.lat, lot.geo.lng);
          var avocado = [{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#aee2e0"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#abce83"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#769E72"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#7B8758"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#EBF4A4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#8dab68"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5B5B3F"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ABCE83"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#A4C67D"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#9BBF72"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#EBF4A4"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#87ae79"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#7f2200"},{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":4.1}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#495421"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"off"}]}];
          var viewOptions = { scrollwheel: false, draggable: false, zoom: 7, disableDefaultUI: true, center: myLatlng, styles: avocado }
          var map = new google.maps.Map(document.getElementById('map'), viewOptions);
      });
    };

    drawMap();

    //get geodata
    // this.getLocation = function(query) {
    //   var self = this;
    //   geoService.geocode(query)
    //     .then(function(res) {
    //       self.lot.lat = res.data.results[0].geometry.location.lat;
    //       self.lot.lng = res.data.results[0].geometry.location.lng;
    //       self.lot.origin = res.data.results[1].formatted_address;
    //       self.lot.country = res.data.results[1].address_components[2].long_name;
    //       geoService.elevation(self.lot.lat, self.lot.lng)
    //         .then(function(res) {
    //           self.lot.altitude = res.data.results[0].elevation;
    //         })
    //     })
    // };

  };

  angular
    .module('lotModule', [])

  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('lot', {
        url: '/:id',
        templateUrl: 'views/lot.html',
        controllerAs: 'view',
        controller: 'lotController',
        resolve: {
          authenticate: function($q, $state, userSession, loginService) {
            var d = $q.defer();
            return loginService.check()
              .then(function(res) {
                if (res.data !== '0') {
                  //authenticated - set constants
                  userSession.username = res.data.username;
                  userSession.email = res.data.email;
                  userSession.firstName = res.data.firstName;
                  userSession.lastName = res.data.lastName;
                  userSession.joined = res.data.joined;
                  userSession.lastLogin = res.data.lastLogin;
                  d.resolve();
                } else {
                  //not authenticated - send error
                  d.reject();
                  $state.go('login');
                }
              });
            return d.promise;
          },
          lot: function(lotService, $stateParams) {
            return lotService.getOne($stateParams.id)
              .then(function(res) {
                return res.data;
              });
          }
        }
      });
  }])

  .controller('lotController', [
    '$state',
    'userSession',
    'lotService',
    'loginService',
    'geoService',
    'NgMap',
    'lot',
    lotController
  ]);

})();
