(function() {
  'use strict';

  var loginController = function($state, userSession, loginService) {

    userSession.loggedIn = false;

    this.userLogin = function() {
      //login details
      var user = {
        username: this.username,
        password: this.password
      };
      loginService.login(user)
        .then(function(res) {
          $state.go('dashboard');
          userSession.loggedIn = true;
        });
    };

  };

  angular
    .module('loginModule', [])

  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controllerAs: 'login',
        controller: 'loginController'
      });
  }])

  .controller('loginController', [
    '$state',
    'userSession',
    'loginService',
    loginController
  ]);

})();
