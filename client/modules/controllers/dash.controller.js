(function() {
  'use strict';

  var dashController = function($state, userSession, lotService, geoService, lots) {

    userSession.loggedIn = true;

    //get lots
    this.lots = lots;
    //set current date
    this.date = new Date();
    //set user constant
    this.user = userSession.firstName;

    //create
    this.createLot = function(lot) {
      //apply static variables
      lot.added = this.date;
      lot.addedBy = this.user;
      //find geolocation of origin
      geoService.geocode(lot.origin)
        .then(function(res) {
          for (var i = 0; i < res.length; i++) {
            lot.lat = res[i].geometry.location.lat();
            lot.lng = res[i].geometry.location.lng();
            lot.origin = res[i].formatted_address;
            for (var k = 0; k < res[i].address_components.length; k++) {
              if (res[i].address_components[k].types[0] == 'country')
                lot.country = res[i].address_components[k].long_name;
            }
          }
          geoService.elevation(lot.lat, lot.lng)
            .then(function(res) {
              for (var i = 0; i < res.length; i++) {
                lot.altitude = res[i].elevation;
              }
              lotService.createOne(lot)
                .then(function(res) {
                  $state.reload();
                });
            });
        });
    };

    //get all
    this.getLots = function() {
      var self = this;
      lotService.getAll()
        .then(function(res) {
          console.log('getAll');
          self.lots = res.data;
        });
    };

  };

  angular
    .module('dashModule', [])

  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: 'views/dashboard.html',
        controllerAs: 'list',
        controller: 'dashController',
        resolve: {
          authenticate: function($q, $state, userSession, loginService) {
            var d = $q.defer();
            return loginService.check()
              .then(function(res) {
                if (res.data !== '0') {
                  //authenticated - set constants
                  userSession.username = res.data.username;
                  userSession.email = res.data.email;
                  userSession.firstName = res.data.firstName;
                  userSession.lastName = res.data.lastName;
                  userSession.joined = res.data.joined;
                  userSession.lastLogin = res.data.lastLogin;
                  d.resolve();
                } else {
                  //not authenticated - send error
                  d.reject();
                  $state.go('login');
                }
              });
            return d.promise;
          },
          lots: function(lotService) {
            return lotService.getAll()
              .then(function(res) {
                return res.data;
              });
          }
        }
      });

  }])

  .controller('dashController', [
    '$state',
    'userSession',
    'lotService',
    'geoService',
    'lots',
    dashController
  ]);

})();
