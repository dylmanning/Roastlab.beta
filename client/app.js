(function() {
  'use strict';

  var globalController = function($state, $scope, $http, userSession, loginService) {

    //watch
    $scope.$on('$stateChangeSuccess', function() {
      $scope.loggedIn = userSession.loggedIn;
      $scope.user = userSession.firstName;
    });

    $scope.loggedIn = userSession.loggedIn;
    $scope.user = userSession.firstName;

    //logout
    $scope.userLogout = function() {
      loginService.logout()
        .then(function(res) {
          userSession.loggedIn = false;
          $state.go('login');
        });
    };

    //send feedback
    $scope.sendFeedback = function(message) {
      message.user = $scope.user;
      $http.post('/api/feedback', message)
        .success(function(res) {
          $scope.feedback = {};
        });
    };

  };

  angular
    .module('roastlab', [
      'ui.router',
      'ui.bootstrap',
      'ngMap',
      'ngAnimate',
      'loginModule',
      'dashModule',
      'lotModule'
    ])

  .config([
      '$urlRouterProvider',
      '$httpProvider',
      function($urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/login');
        $httpProvider.interceptors.push('loadingService');
      }
    ])

    .controller('globalController', [
      '$state',
      '$scope',
      '$http',
      'userSession',
      'loginService',
      globalController
    ]);

})();
