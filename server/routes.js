var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var Lot = require('./models/lot/lot.model');
var sendgrid  = require('sendgrid')('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');

module.exports = function(app) {

  //authentication api
  app.post('/api/login', passport.authenticate('local'), function(req, res) {
    res.sendStatus(200);
  });

  app.post('/api/logout', function(req, res) {
    req.logout();
    res.sendStatus(200);
  });

  app.get('/api/loggedin', function(req, res) {
    res.send(req.isAuthenticated() ? req.user : '0');
  });

  //lot api
  app.get('/api/lots', function(req, res) {
    Lot.find({}, function(err, docs) {
      res.json(docs);
    });
  });

  app.get('/api/lot/:id', function(req, res) {
    Lot.findById(req.params.id, function(err, docs) {
      res.json(docs);
    });
  });

  app.delete('/api/lot/:id', function(req, res) {
    Lot.findByIdAndRemove(req.params.id, function(err, docs) {
      res.json(docs);
    });
  });

  app.post('/api/lot', function(req, res) {
    Lot.create({
      name:       req.body.name,
      assignment: req.body.assignment,
      rating:     req.body.rating,
      added:      req.body.added,
      addedBy:    req.body.addedBy,
      origin:     req.body.origin,
      country:    req.body.country,
      farmer:     req.body.farmer,
      geo: {
        lat:      req.body.lat,
        lng:      req.body.lng,
        altitude: req.body.altitude
      }
    }, function(err) {
      if (err) { return console.error(err); }
      if (!err) { res.sendStatus(201); }
    });
  });

  app.put('/api/lot/:id', function(req, res) {
    var query = { _id: req.params.id };
    Lot.findOneAndUpdate(query, req.body, function(err) {
      if (err) { return console.error(err); }
      if (!err) { res.sendStatus(200); }
    });
  });

  //email api
  app.post('/api/feedback', function(req, res) {
    var feedback = new sendgrid.Email();
    feedback.to = 'dyl.manning@gmail.com';
    feedback.subject = 'Feedback from ' + req.body.user;
    feedback.from = 'feedback@roastlab.beta';
    feedback.html = '<h2><small>module</small>' + req.body.module + '</h2><br />' + '<p>' + req.body.content + '</p><br />' + 'regards, ' + req.body.user ;
    feedback.addFilter('templates', 'enable', 1);
    feedback.addFilter('templates', 'template_id', 'ef6875d0-b7d2-44c1-a053-94f4761c4375');
    sendgrid.send(feedback, function(err, json) {
      if (err) { return console.error(err); }
      if (!err) { res.sendStatus(200); }
    });
  });

};
