'use strict';

//set default environment
var express = require('express');
var mongoose = require('mongoose');

//connect to database (test)
mongoose.connect('mongodb://localhost/test');

//populate db with initial database
var seed = require('./config/seed');

//setup server
var app = express();
var server = require('http').createServer(app);

//server configuration
require('./config/express')(app);

//api routes
require('./routes')(app);

//start server
server.listen(3000, 'localhost', function() {
  console.log('Server listening on', 3000);
});

//expose app
exports = module.exports = app;
