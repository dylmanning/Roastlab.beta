'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var farmerSchema = new Schema({
  name: String,
  location: String
});

module.exports = mongoose.model('Farmer', farmerSchema);
