'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  password: String,
  email: {
    type: String,
    lowercase: true,
    unique: true
  },
  firstName: String,
  lastName: String,
  joined: Date,
  lastLogin: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('User', userSchema);
