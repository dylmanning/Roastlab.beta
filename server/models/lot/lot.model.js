'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lotSchema = new Schema({
  name: String,
  rating: { type: Number, default: 0 },
  assignment: String,
  origin: String,
  country: String,
  farmer: String,
  addedBy: String,
  added: Date,
  updated: { type: Date, default: new Date() },
  updatedBy: String,
  state: { type: Number, default: 0 },
  group: String,
  visits: [{ date: Date, visitNo: { type: Number, default: 1 } }],
  info: {
    connection: String,
    history: String,
    comments: String,
    production: String
  },
  metrics: {
    cropId: String,
    lotNo: String,
    lotSz: String,
    bCode: String
  },
  geo: {
    lat: Number,
    lng: Number,
    altitude: Number,
    climate: String
  },
  assesments : [{
    name: String,
    url: String,
    assesmentDate: Date
  }],
  production: [{
    name: String,
    url: String,
    assesmentDate: Date
  }]
});

module.exports = mongoose.model('Lot', lotSchema);
